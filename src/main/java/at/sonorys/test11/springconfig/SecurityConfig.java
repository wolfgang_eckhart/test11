package at.sonorys.test11.springconfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void configure(HttpSecurity http) throws Exception {


		http	
			.authorizeRequests()
				.antMatchers("/*").permitAll()
				.antMatchers("/test11/**").permitAll()
				.antMatchers("/testwe/**").permitAll()
				.antMatchers("/tests/inc/**").permitAll()
				.antMatchers("/metrics/**").permitAll()
				.antMatchers("/actuator/**").permitAll()
				.antMatchers("/api/rest/actuator/**").permitAll()
				.antMatchers("/api/rest/**").permitAll()
				.anyRequest()
					.authenticated()
					.and()
				.formLogin()
					.loginPage("/login")
					.permitAll()
					.and()

				.logout()
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/");



	}


}
