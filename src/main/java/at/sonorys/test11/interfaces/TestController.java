package at.sonorys.test11.interfaces;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {


  private Logger logger = LoggerFactory.getLogger(this.getClass());


  @RequestMapping("/")
  public String index() {

    return "Greetings from Spring Boot!";
  }


  @RequestMapping(value = "/test11", method = RequestMethod.GET)
  public String test11() {

    logger.info("test11 - begin...");
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info("test11 - finished...");

    return "Success";
  }



}
